public class Ros.Application : Gtk.Application {
    public Gtk.Window application_window;

    public Application () {
        Object (
            application_id: "com.gitlab.gdeflaux.regolith-on-stage",
            flags: ApplicationFlags.FLAGS_NONE
        );
    }

    protected override void activate () {
      var application_window = new Ros.ApplicationWindow(this);
      add_window (application_window);
    }
}

public class Ros.ApplicationWindow : Gtk.Window {
  public Ros.DesktopConfigBox desktop_config_box;
  public Gtk.CssProvider      css_provider;

  public ApplicationWindow(Application app) {
    application = app;
    style_window();

    var outter_box = new Gtk.Box(Gtk.Orientation.VERTICAL, 10);
    add(outter_box);

    var stack = new Gtk.Stack();
    stack.margin = 5;
    desktop_config_box = new Ros.DesktopConfigBox();
    //stack.set_transition_type(Gtk.StackTransitionType.NONE);
    //stack.set_transition_duration(300);
    //stack.add_titled(new Gtk.Label("Coming Soon!"), "themes", "Themes");
    stack.add_titled(desktop_config_box, "variables", "Desktop Config");
    //stack.add_titled(new Gtk.Label("Coming Soon!"), "i3blocs", "i3Blocks");

    var stack_switcher = new Gtk.StackSwitcher();
    stack_switcher.set_stack(stack);
    stack_switcher.halign = Gtk.Align.CENTER;
    stack_switcher.margin_top = 10;

    outter_box.pack_start(stack_switcher, false, false, 0);
    outter_box.pack_start(stack, true, true, 0);

    show_all();
  }

  private void style_window() {
    var headerbar = new Ros.HeaderBar (this);
    set_titlebar (headerbar);
    window_position = Gtk.WindowPosition.CENTER;
    default_height = 800;
    default_width = 1400;
    resizable = true;

    Ros.StyleHelper.load_stylesheet_from_resource("/css/style.css");

    this.icon = new Gdk.Pixbuf.from_resource("/logo/logo-ros-icon.svg");
  }
}
