using Gee;

public class Ros.XResourcesParser {
  public static void parse(string[] filenames, HashMap<string, string> *macros, HashMap<string, Ros.VariableValue> *variables) {
    //var     file = File.new_for_path(filename);
    string  line;
    File    file_to_parse = null;

    stdout.printf("\nParsing theme file...\n");
    foreach (var filename in filenames) {
      var file = File.new_for_path(filename);
      if (!file.query_exists()) {
        stdout.printf ("  File '%s' doesn't exist.\n", file.get_path());
      } else {
        file_to_parse = file;
        break;
      }
    }

    if (file_to_parse == null) {
        stderr.printf("  No theme file found!\n"); // this should generate an exception instead
    } else {
      stdout.printf("  Using '%s'\n", file_to_parse.get_path());
      var stream = new DataInputStream (file_to_parse.read ());
      while ((line = stream.read_line (null)) != null) {
          line = line.strip();
          if (!is_comment(line) && line.length > 0) {
            if (is_include(line)) {
              var incl_file = parse_include(line);
              parse({incl_file}, macros, variables);
            } else if (is_macro(line)) {
              var k = parse_macro(line);
              macros->set(k[0], k[1]);
            } else {
              var v = parse_variable(line);
              var vv = new Ros.VariableValue();
              vv.value = v[1];
              vv.default_value = v[1];
              variables->set(v[0], vv);
            }
          }
      }
    }
  }

  public static void parse_user_definitions(string filename, HashMap<string, Ros.VariableValue> *variables) {
    var     file = File.new_for_path (filename);
    string  line;

    stdout.printf("\nParsing user definitions file...\n");
    if (!file.query_exists ()) {
        stdout.printf ("  File '%s' doesn't exist (not a problem).\n", file.get_path());
    } else {
      stdout.printf("  Using '%s'\n", file.get_path());
      var stream = new DataInputStream (file.read ());
      while ((line = stream.read_line (null)) != null) {
          line = line.strip();
          if (!is_comment(line) && line.length > 0) {
            if (is_macro(line)) {
              // Do nothing
            } else {
              var v = parse_variable(line);
              variables->get(v[0]).value = v[1];
              variables->get(v[0]).is_user_defined = true;
            }
          }
        }
      }
    }

  private static bool is_include(string line) {
    return line.has_prefix("#include ");
  }

  private static string parse_include(string line) {
    Regex     regex = new Regex ("#include\\s+\"(.*)\"");

    return regex.replace(line, line.length, 0, "\\1");
  }

  private static bool is_macro(string line) {
    return line.has_prefix("#define ");
  }

  private static string[] parse_macro(string line) {
    Regex regex_macro = new Regex ("(#define)\\s+([^\\s]*)\\s+(.*)");
    Regex regex_function = new Regex ("(#define)\\s+([a-zA-Z_][a-zA-Z_]*\\(.*\\))\\s+(.*)");
    Regex regex;

    if (regex_function.match(line)) {
      regex = regex_function;
    } else {
      regex = regex_macro;
    }

    var sp = regex.split(line);
    return { sp[2], sp[3] };
  }

  private static string[] parse_variable(string line) {
    Regex     regex = new Regex ("([^:]*):\\s*(.*)");
    string[]  sp = regex.split(line);
    string[]  v = { sp[1], sp[2] };

    return v;
  }

  private static bool is_comment(string line) {
    return (line[0] == '!');
  }
}
