using Gee;

public class Ros.I3xrocksParser {
  public static void parse(string dirname, HashMap<string, string> *macros, HashMap<string, Ros.VariableValue> *variables) {
    var     dir = File.new_for_path (dirname);
    string  line;

    stdout.printf("\nParsing i3xrocks folder...\n");
    if (!dir.query_exists ()) {
        stderr.printf ("  Folder '%s' doesn't exist.\n", dir.get_path());
    } else {
      var       enumerator = dir.enumerate_children (FileAttribute.STANDARD_NAME, 0);
      FileInfo  file_info;

      while ((file_info = enumerator.next_file ()) != null) {
        var file = File.new_for_path (dirname + file_info.get_name());
        var stream = new DataInputStream (file.read ());
        //var count = 0;

        stdout.printf("  Parsing '%s%s'\n", dirname, file_info.get_name());
        while ((line = stream.read_line (null)) != null) {
            line = line.strip();
            if (!is_comment(line) && line.length > 0) {
              if (is_variable(line)) {
                //count++;
                var v = parse_variable(line);
                if (!variables->has_key(v[0])) {
                  //stdout.printf("  New Var: [%s] - [%s]\n", v[0], v[1]);
                  var vv = new Ros.VariableValue();
                  vv.value = v[1];
                  vv.default_value = v[1];
                  variables->set(v[0], vv);
                }
              }
            }
        }
        //stdout.printf("  Total Match: %d\n\n", count);
      }
    }
  }

  private static bool is_comment(string line) {
    return (line[0] == '#');
  }

  private static bool is_variable(string line) {
    return line.contains("xrescat ");
  }

  private static string[] parse_variable(string line) {
    Regex     regex = new Regex ("(`(xrescat)\\s+([^\\s]+)(.*)?`)|(\\$\\((xrescat)\\s+([^\\s]+)(.*)?\\))");
    string[]  sp = regex.split(line);

    /* for (int i = 0; i < sp.length; i++) {
      stdout.printf("sp[%d]: [%s]\n", i, sp[i]);
    }
    stdout.printf("\n"); */

    if (sp[1] != "") {
      return { sp[3], sp[4].strip() };
    } else {
      return { sp[7], sp[8].strip() };
    }
  }

}
