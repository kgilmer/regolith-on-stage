using Gee;

public class Ros.VariableValue {
  public string value;
  public string default_value;
  public bool   is_macro = false;
  public bool   is_user_defined = false;

  public void reset(HashMap<string,string> macros) {
    value = default_value;
    is_macro = macros.has_key(value);
    is_user_defined = false;
  }

  public void update(string new_value, HashMap<string,string> macros) {
    value = new_value;
    is_macro = macros.has_key(new_value);
    is_user_defined = (new_value != default_value);
  }
}

public class Ros.ParserManager {
  public HashMap<string, string>            *macros;
  public HashMap<string, Ros.VariableValue> *variables;

  public ParserManager(HashMap<string, string> *macros, HashMap<string, Ros.VariableValue> *variables) {
    this.macros = macros;
    this.variables = variables;
  }

  public void parse() {

    XResourcesParser.parse({ theme_user_file(), theme_system_file() }, macros, variables);
    check_variables_for_macros();

    I3ConfigParser.parse({ i3_config_user_file(), i3_config_system_file() }, macros, variables);

    I3xrocksParser.parse(i3xrocks_dir(), macros, variables);

    XResourcesParser.parse_user_definitions(user_definitions_file(), variables);
    check_variables_for_macros(true);
  }

  public void print_macros() {
    foreach (var macro in macros->entries) {
        stdout.printf ("[%s] => [%s]\n", macro.key, macro.value);
    }
  }

  public void print_variables(bool user_definitions_only = false) {
    foreach (var variable in variables->entries) {
      if (variable.value.is_user_defined || !user_definitions_only) {
        if (variable.value.is_macro) {
          stdout.printf ("[%s] => [%s] => [%s]\n", variable.key, variable.value.value, macros->get(variable.value.value));
        } else {
          stdout.printf ("[%s] => [%s]\n", variable.key, variable.value.value);
        }
      }
    }
  }

  private void check_variables_for_macros(bool user_definitions_only = false) {
    bool turned_macro = false;

    foreach (var variable in variables->entries) {
      if (variable.value.is_user_defined || !user_definitions_only) {
        foreach (var macro in macros->entries) {
          turned_macro = false;
          if (variable.value.value == macro.key) {
            variable.value.is_macro = true;
            turned_macro = true;
            break;
          }
        }
        variable.value.is_macro = turned_macro;
      }
    }
  }

  public void to_file(HashMap<string, Ros.VariableValue> variables, HashMap<string, string> macros) {
    var file = File.new_for_path (user_definitions_file());

    if (file.query_exists ()) {
      file.delete ();
    }

    var dos = new DataOutputStream (file.create(FileCreateFlags.REPLACE_DESTINATION));

    dos.put_string("! -- Macros\n");

    foreach (var macro in macros.entries) {
      dos.put_string("#define " + macro.key + " " + macro.value + "\n");
    }

    dos.put_string("\n");
    dos.put_string("! -- Custom variables\n");

    foreach (var variable in variables.entries) {
      if (variable.value.is_user_defined) {
        dos.put_string(variable.key + ": " + variable.value.value + "\n");
      }
    }
  }

  private static string theme_system_file() {
    return "/etc/regolith/styles/root";
  }

  private static string theme_user_file() {
    var home_dir = Environ.get_variable(Environ.get(), "HOME");
    return home_dir + "/.Xresources-regolith";
  }

  private static string user_definitions_file() {
    var home_dir = Environ.get_variable(Environ.get(), "HOME");
    return home_dir + "/.config/regolith/Xresources";
  }

  private static string i3_config_system_file() {
    return "/etc/regolith/i3/config";
  }

  private static string i3_config_user_file() {
    var home_dir = Environ.get_variable(Environ.get(), "HOME");
    return home_dir + "/.config/regolith/i3/config";
  }

  private static string i3xrocks_dir() {
    return "/usr/share/i3xrocks/"; // Trailing slash required
  }
}
