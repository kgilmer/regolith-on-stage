public class Ros.MacrosWindow : Gtk.Window {
  public Ros.DesktopConfigBox desktop_config;
  public Gtk.SearchEntry      filter_entry;
  public Gtk.TreeModelFilter  filter_model;

  public MacrosWindow(Ros.DesktopConfigBox desktop_config, Gtk.Entry variable_value) {
    this.desktop_config = desktop_config;
    style_window();

    var vbox = new Gtk.Box(Gtk.Orientation.VERTICAL, 10);
    vbox.margin = 10;
    this.add(vbox);

    // Filter Entry
    filter_entry = new Gtk.SearchEntry();
    filter_entry.halign = Gtk.Align.CENTER;
    filter_entry.changed.connect(() => {
      filter_model.refilter();
    });
    vbox.pack_start(filter_entry, false, false, 0);

    // Tree View
    var scrolled_window = new Gtk.ScrolledWindow(null, null);
    vbox.pack_start(scrolled_window, true, true, 0);
    var tree = new Gtk.TreeView();
    scrolled_window.add(tree);

      // Data store
    var store = new Gtk.ListStore(2, typeof (string), typeof (string));
    var crt_macro_value = new Gtk.CellRendererText ();
    crt_macro_value.font = desktop_config.get_i3bar_icon_font();
    tree.insert_column_with_attributes (-1, "Name", new Gtk.CellRendererText (), "text", 0);
    tree.insert_column_with_attributes (-1, "Value", crt_macro_value, "text", 1);

    var c = tree.get_column(0);
    c.set_sort_column_id(0);
    c.set_sort_order(Gtk.SortType.ASCENDING);
    c = tree.get_column(1);
    c.set_sort_column_id(1);

    Gtk.TreeIter iter;
    foreach (var macro in desktop_config.macros.entries) {
      store.append (out iter);
			store.set (iter, 0, macro.key, 1, macro.value);
    }

      // Filters
    filter_model = new Gtk.TreeModelFilter(store, null);
    filter_model.set_visible_func(filter_tree);
    var sort_model = new Gtk.TreeModelSort.with_model(filter_model);
    tree.set_model(sort_model);

      // Selection mode
    var selection = tree.get_selection();
    selection.set_mode(Gtk.SelectionMode.SINGLE);
    tree.activate_on_single_click = false;
    tree.row_activated.connect(() => {
      Gtk.TreeModel data;
		  Gtk.TreeIter  it;
      string        macro_key;

      if (selection.get_selected(out data, out it)) {
        data.get(it, 0, out macro_key);

        var pos = variable_value.cursor_position;
        var len = variable_value.text.length;
        var t1 = variable_value.text[0:pos];
        var t2 = variable_value.text[pos:len];
        variable_value.text = t1 + macro_key + t2;
        variable_value.set_position(t1.length + macro_key.length);
        this.close();
      }
    });

    show_all();
  }

  private void style_window() {
    modal = true;
    set_size_request(650, 500);
    this.icon = new Gdk.Pixbuf.from_resource("/logo/logo-ros-icon.svg");

    var hb = new Gtk.HeaderBar();
    hb.title = "Macro Explorer";
    hb.subtitle = "Double click on a macro to insert it";
    hb.show_close_button = true;
    this.set_titlebar(hb);
  }

  private bool filter_tree(Gtk.TreeModel model, Gtk.TreeIter iter) {
      GLib.Value mk;
      string macro_key;

      model.get_value(iter, 0, out mk);
      macro_key = mk.get_string();

      if (filter_entry.text == "")
          return true;

      if (macro_key.index_of(filter_entry.text) > -1)
          return true;
      else
          return false;
  }
}
