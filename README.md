<p align="center">
  <img width="100" height="100" src="data/logo/logo-ros.svg">
</p>

# Regolith On Stage

Regolith On Stage is a GTK application that allows you to customize your [Regolith](https://regolith-linux.org/) desktop environment without having to edit configuration files manually.

> Ricing without Staging!

Visit the [website](https://regolith-on-stage.gitlab.io) to learn more about the features of the application.

## In a Hurry?

Download the latest version from the [releases page](https://gitlab.com/regolith-on-stage/regolith-on-stage/-/releases).

Visit the [wiki](https://gitlab.com/regolith-on-stage/regolith-on-stage/-/wikis/home) for more information about how the application works and technical documentation.

### Compile & Run

The application is written in [Vala](https://wiki.gnome.org/Projects/Vala) and is built with [Meson](https://mesonbuild.com/).

```
$ git clone https://gitlab.com/regolith-on-stage/regolith-on-stage.git
$ cd regolith-on-stage
$ meson build
$ cd build
$ ninja
$ ./regolith-on-stage
```

If you want to install the application run `meson install` from the `build` directory.

### Preview

![screenshot](screenshot.png)
